#include <stdio.h>
#include <stdlib.h>

long int factorial (int n)
{
	if (n <= 1)
		return 1;
	else 
		return factorial(n-1) * n;
}
