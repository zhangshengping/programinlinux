#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main()
{
	int fd;
	fd = open("file",O_WRONLY|O_CREAT|O_TRUNC,S_IWUSR|S_IRUSR|S_IWGRP|S_IRGRP|S_IROTH);
	if (-1 == fd)
	{
		printf("file open or create error");
	}
	return 0;
}
