



#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>

int main()
{
	char ch[100];
	ssize_t nr;
	int fd;
	printf ("ssize_t %ld unsigned long %ld int %ld\n",sizeof(ssize_t),sizeof(unsigned long),sizeof(int));
	fd = open("file",O_RDONLY);
	nr = read(fd,&ch,20);
	printf("文件id=%ld, 读取文件时的返回值是%ld\n",fd,nr);
	if(-1 == nr)
		printf("file read error");
	else 
		printf(ch);
	return 0;
}
