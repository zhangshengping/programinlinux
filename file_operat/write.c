#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

int main()
{
	int fd;
	const char *buf = "My ship is solid!";
	ssize_t nr;
	fd = open("file",O_WRONLY|O_CREAT|O_TRUNC,S_IWUSR|S_IRUSR|S_IWGRP|S_IRGRP|S_IROTH);
	nr = write(fd,buf,strlen(buf));
	if(nr == -1)
	{
		printf("\nerror\n");
		return -1;
	}
	return 0;
}
