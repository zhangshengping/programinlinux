#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>

int main()
{
	unsigned long word;
	ssize_t nr;
	int fd;
	printf ("ssize_t %ld\n unsigned long %ld\n int %ld\n",sizeof(ssize_t),sizeof(unsigned long),sizeof(int));
	fd = open("file",O_RDONLY);
	nr = read(fd,&word,sizeof(unsigned long));
	printf("%ld, %ld\n",fd,nr);
	if(-1 == nr)
		printf("file read error");
	else 
		printf((char*)&word);
	return 0;
}
